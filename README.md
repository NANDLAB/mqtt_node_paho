# Mqtt Node Paho
A base class for mqtt nodes, which uses [paho.mqtt.cpp](https://github.com/eclipse/paho.mqtt.cpp/tree/master) and [Boost ASIO](https://www.boost.org/doc/libs/release/libs/asio/).

A mqtt nodes status (ONLINE or OFFLINE) can be tracked by its name. The program exits if it receives a mqtt kill message or system term signal.

The following build instruction assumes you have a Debian-based Linux distribution.

# Requirements
## Boost System
Install the official Boost system developement package with apt:
```
sudo apt install libboost-system-dev
```

## paho.mqtt.cpp
Unfortunately, there is no official apt repository with a deb package, so we have to build one ourselfves.

First, we have to build [paho.mqtt.c](https://github.com/eclipse/paho.mqtt.c), because [paho.mqtt.cpp](https://github.com/eclipse/paho.mqtt.cpp/tree/master) depends on it. We will build and install a deb package.

To use `PAHO_WITH_SSL=ON` we need OpenSSL: `sudo apt install libssl-dev`.

```
git clone https://github.com/eclipse/paho.mqtt.c.git
cd paho.mqtt.c
mkdir build
cd build

cmake -DPAHO_BUILD_SHARED=ON -DPAHO_BUILD_STATIC=ON -DPAHO_HIGH_PERFORMANCE=ON -DPAHO_WITH_SSL=ON -DPAHO_BUILD_DOCUMENTATION=OFF -DPAHO_BUILD_SAMPLES=OFF -DPAHO_BUILD_DEB_PACKAGE=ON ..
make
cpack
sudo apt install ./*.deb
sudo ldconfig
```

You can go back to the previous directory: `cd ../..`.

Now, we will build and install the [paho.mqtt.cpp](https://github.com/eclipse/paho.mqtt.cpp/tree/master) deb package:

```
git clone https://github.com/eclipse/paho.mqtt.cpp.git
cd paho.mqtt.cpp
mkdir build
cd build

cmake -DPAHO_BUILD_SHARED=ON -DPAHO_BUILD_STATIC=ON -DPAHO_WITH_SSL=ON -DPAHO_BUILD_DOCUMENTATION=OFF -DPAHO_BUILD_SAMPLES=OFF -DPAHO_BUILD_TESTS=OFF -DPAHO_BUILD_DEB_PACKAGE=ON ..
make
cpack
sudo apt install ./*.deb
sudo ldconfig
```

On subsequent building, if there are multiple deb package versions, this command installs the latest one:
```
sudo apt install $(ls -vr ./*.deb | head -1)
```


# Build Mqtt Node Paho
If you are using mqtt_node_paho as a git subproject, ignore this step. This builds the mqtt_node_paho static library:
```
git clone https://gitlab.com/NANDLAB/mqtt_node_paho.git
cd mqtt_node_paho
mkdir build
cd build

cmake ..
make
```
